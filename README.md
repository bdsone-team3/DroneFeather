# Library Dependencies
These are the arduino libraries used in this sketch.
* [RadioHead RFM9x Library](https://learn.adafruit.com/adafruit-feather-32u4-radio-with-lora-radio-module/using-the-rfm-9x-radio#radiohead-rfm9x-library-example)
* [Adafruit_BNO055 Library](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/arduino-code#download-the-driver-from-github)
* [Adafruit_Sensor Library](https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/arduino-code#download-adafruit-sensor)