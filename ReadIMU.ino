#include <RH_RF95.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>

#define SAMPLE_RATE_DELAY (100)

#define RFM95_CS 8
#define RFM95_RST 4
#define RFM95_INT 7
#define RF95_FREQ 915.0

Adafruit_BNO055 bno = Adafruit_BNO055(55);

RH_RF95 rf95(RFM95_CS, RFM95_INT);

static constexpr char SEP = '|';

struct DataType
{
  float accelX;
  float accelY;
  float accelZ;
  
  float magX;
  float magY;
  float magZ;

  float gyroX;
  float gyroY;
  float gyroZ;
  
  float eulerX;
  float eulerY;
  float eulerZ;

  float linAccelX;
  float linAccelY;
  float linAccelZ;

  float gravityX;
  float gravityY;
  float gravityZ;
};

void setupIMU()
{
  if (!bno.begin())
  {
    Serial.print("IMU not detected.");
    while(1);
  }

  delay(1000);

  bno.setExtCrystalUse(true);
}

void loopIMU()
{
  DataType data = DataType();
  imu::Vector<3> vec;
  
  vec = bno.getVector(Adafruit_BNO055::VECTOR_ACCELEROMETER);
  data.accelX = vec.x();
  data.accelY = vec.y();
  data.accelZ = vec.z();

  vec = bno.getVector(Adafruit_BNO055::VECTOR_MAGNETOMETER);
  data.magX = vec.x();
  data.magY = vec.y();
  data.magZ = vec.z();

  vec = bno.getVector(Adafruit_BNO055::VECTOR_GYROSCOPE);
  data.gyroX = vec.x();
  data.gyroY = vec.y();
  data.gyroZ = vec.z();
    
  vec = bno.getVector(Adafruit_BNO055::VECTOR_EULER);
  data.eulerX = vec.x();
  data.eulerY = vec.y();
  data.eulerZ = vec.z();

  vec = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
  data.linAccelX = vec.x();
  data.linAccelY = vec.y();
  data.linAccelZ = vec.z();

  vec = bno.getVector(Adafruit_BNO055::VECTOR_GRAVITY);
  data.gravityX = vec.x();
  data.gravityY = vec.y();
  data.gravityZ = vec.z();

  unsigned int bufferSize = sizeof(data) + 1;
  char* buffer = new char[bufferSize];
  memcpy(buffer, &data, bufferSize - 1);
  buffer[bufferSize - 1] = SEP; // message separator
  rf95.send(buffer, bufferSize);
  rf95.waitPacketSent();
  delete buffer;
}

void setupUblox()
{

}

void loopUblox()
{

}

void setupRadio()
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);

  delay(100);

  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);

  while (!rf95.init())
  {
    Serial.println("failed radio init");
    while (1);
  }

  if (!rf95.setFrequency(RF95_FREQ))
  {
    Serial.println("failed to set freq");
    while (1);
  }

  rf95.setTxPower(23, false);
}

void setup()
{
  setupIMU();

  setupUblox();

  setupRadio();
}

void loop()
{
  loopIMU();

  loopUblox();
}
